//
//  File.swift
//  
//
//  Created by Ruslan on 9/30/19.
//

import UIKit

extension UICollectionViewCell {
    static var empty: UICollectionViewCell {
        return UICollectionViewCell(frame: CGRect.null)
    }
}
