//
//  File.swift
//  
//
//  Created by Ruslan on 9/30/19.
//

extension Float {
    var formattedPrice: String {
        return String(format: "$ %.2f", self)
    }
}
