//
//  File.swift
//  
//
//  Created by Ruslan on 9/30/19.
//

import UIKit

extension UITableView {
    func removeEmptyCells() {
        self.tableFooterView = UIView()
    }
    
    func registerCell(_ name: String) {
        self.register(UINib(nibName: name, bundle: nil), forCellReuseIdentifier: name)
    }
    
    func registerCells(_ names: [String]) {
        for name in names {
            registerCell(name)
        }
    }
    
    func dequeueCell<T: UITableViewCell>(_ indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withIdentifier: String(describing: T.self), for: indexPath) as! T
    }
}
