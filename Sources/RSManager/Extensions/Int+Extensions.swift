//
//  File.swift
//  
//
//  Created by Ruslan on 9/30/19.
//

extension Int {
    var formattedMinutes: String {
        return String(format: "%i min", self)
    }
}
