//
//  File.swift
//  
//
//  Created by Ruslan on 9/30/19.
//

import Foundation

extension Date {
    func adding(days: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: days, to: self)!
    }
    func near(days: Int) -> [Date] {
        return days == 0 ? [Date()] : (0...abs(days)).map {
            adding(days: $0 * (days < 0 ? -1 : 1) )
        }
    }
    func inSameDayAs(_ date: Date) -> Bool {
        return Calendar.current.isDate(self, inSameDayAs: date)
    }
    
    // MARK: - Format
    
    func formatted(_ format: RSDateTimeFormat) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format.rawValue
//        formatter.timeZone = RSConstants.DateTime.zone.input
        return formatter.string(from: self)
    }
    
    // MARK: - Props
    
    var day: Int { return Calendar.current.component(.day, from: self) }
}
