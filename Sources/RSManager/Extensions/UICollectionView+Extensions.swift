//
//  File.swift
//  
//
//  Created by Ruslan on 9/30/19.
//

import UIKit

extension UICollectionView {
    func registerCell(_ name: String) {
        self.register(UINib(nibName: name, bundle: nil), forCellWithReuseIdentifier: name)
    }
    
    func registerCells(_ names: [String]) {
        for name in names {
            registerCell(name)
        }
    }
    
    func dequeueCell<T: UICollectionViewCell>(_ indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withReuseIdentifier: String(describing: T.self), for: indexPath) as! T
    }
}
