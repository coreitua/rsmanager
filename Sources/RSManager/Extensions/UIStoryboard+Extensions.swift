//
//  File.swift
//  
//
//  Created by Ruslan on 9/30/19.
//

import UIKit

extension UIStoryboard {
    func instantiateVC<T: UIViewController>() -> T? {
        if let name = NSStringFromClass(T.self).components(separatedBy: ".").last {
            return instantiateViewController(withIdentifier: name) as? T
        }
        return nil
    }
    
    func instantiateID(_ identifier: String) -> UIViewController {
        return instantiateViewController(withIdentifier: identifier)
    }
}
