//
//  File.swift
//  
//
//  Created by Ruslan on 9/30/19.
//

import UIKit

extension UITableViewCell {
    static var empty: UITableViewCell {
        return UITableViewCell(style: .default, reuseIdentifier: nil)
    }
}
