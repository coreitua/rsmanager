//
//  File.swift
//  
//
//  Created by Ruslan on 9/30/19.
//

import Foundation
import UIKit

public typealias RSAlertAction = ((_ action: UIAlertAction, _ text: String?) -> Void)

public class RSAlert {
    public static func common(title: String?, message: String?, showCancel: Bool = true,
                       okTitle: String? = "btn_ok", okHandler: RSAlertAction? = nil,
                       cancelTitle: String? = "btn_cancel", cancelHandler: ((UIAlertAction) -> Void)? = nil,
                       showsTextField: Bool = false, textFieldPlaceholder: String? = nil, textFieldSecurity: Bool = false)
    {
        let alertController = UIAlertController(title: title?.localized(), message: message?.localized(), preferredStyle: .alert)
        if showsTextField {
            alertController.addTextField { (textField) in
                textField.placeholder = textFieldPlaceholder
                textField.isSecureTextEntry = textFieldSecurity
            }
        }
        if showCancel {
            alertController.addAction(UIAlertAction(title: cancelTitle?.localized(), style: .default, handler: cancelHandler))
        }
        alertController.addAction(UIAlertAction(title: okTitle?.localized(), style: .default) { (action) in
            okHandler?(action, alertController.textFields?.first?.text)
        })
        RSRouter().topController()?.present(alertController, animated: true, completion: nil)
    }

    public static func simple(title: String?, message: String?,
                       okTitle: String? = "btn_ok", okHandler: RSAlertAction? = nil)
    {
        RSAlert.common(title: title, message: message, showCancel: false, okTitle: okTitle, okHandler: okHandler)
    }
    
    public static func error(_ message: String?) {
        RSAlert.simple(title: "btn_error", message: message)
    }
    
    public static func success(_ message: String?) {
        RSAlert.simple(title: "btn_success", message: message)
    }
    
    // MARK: - Text
    
    public static func text(title: String?, message: String?, textFieldPlaceholder: String?, okHandler: RSAlertAction? = nil) {
        RSAlert.common(title: title, message: message, showCancel: false,
                       okHandler: okHandler,
                       showsTextField: true, textFieldPlaceholder: textFieldPlaceholder)
    }
    
    // MARK: - Sheet
    
    public static func sheet(title: String?, message: String?, actions: UIAlertAction...) {
        let alertController = UIAlertController(title: title?.localized(), message: message?.localized(), preferredStyle: .actionSheet)
        for action in actions {
            alertController.addAction(action)
        }
        alertController.addAction(UIAlertAction(title: "btn_cancel".localized(), style: .cancel, handler: nil))
        RSRouter().topController()?.present(alertController, animated: true, completion: nil)
    }
}
