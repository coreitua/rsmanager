//
//  File.swift
//  
//
//  Created by Ruslan on 9/30/19.
//

import Foundation

public class RSConstants {
    struct DateTime {
        struct zone {
            static let input = TimeZone(identifier: "UTC")
            static let output = TimeZone.current
        }
        struct locale {
            static let input = Locale(identifier: "en_US")
            static let output = Locale(identifier: "en_US")
        }
    }
}

public enum RSDateTimeFormat: String {
    case serverInput =  "yyyy-MM-dd HH:mm:ss"
    case serverOutput = "yyyy-MM-dd"
    case weekday = "EE"
    case month = "MMMM yyyy"
    case dateTime = "yyyy/MM/dd HH:mm"
    case time = "HH:mm"
}

public enum RSRouterPresentationType {
    case show, present
}

public enum RSStoryboardName: String {
    case main = "Main"
    case login = "Login"
}
