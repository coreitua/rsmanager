//
//  File.swift
//  
//
//  Created by Ruslan on 9/30/19.
//

import Foundation
import UIKit

open class RSRouter {
    public var rootController: UIViewController? {
        return UIApplication.shared.keyWindow?.rootViewController
    }
    
    public func setRootController(_ controller: UIViewController) {
        UIApplication.shared.keyWindow?.rootViewController = controller
    }
    
    public func topController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topController(controller: presented)
        }
        return controller
    }
    
    public func storyboard(_ name: RSStoryboardName) -> UIStoryboard {
        return UIStoryboard(name: name.rawValue, bundle: nil)
    }
    
    public func show(_ vc: UIViewController, _ from: UIViewController, _ type: RSRouterPresentationType, animated: Bool = true) {
        switch type {
        case .show: from.navigationController?.show(vc, sender: nil)
        case .present: from.present(vc, animated: animated, completion: nil)
        }
    }
}
