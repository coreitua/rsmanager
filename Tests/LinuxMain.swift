import XCTest

import RSManagerTests

var tests = [XCTestCaseEntry]()
tests += RSManagerTests.allTests()
XCTMain(tests)
